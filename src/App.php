<?php

namespace Src;

use Controller\Materials;
use Controller\Types;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

class App
{
    protected $uri;

    public function __construct()
    {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = preg_replace('/[^A-Za-z0-9\/-]/', '', $uri);
        $uri = explode('/', $uri);

        $requestMethod = $_SERVER['REQUEST_METHOD'];

        if (isset($uri[1])) {
            $id = null;

            if (isset($uri[2])) {
                $id = $uri[2];
            }


            switch ($uri[1]) {
                case 'materials':
                    $controller = new Materials($id, $requestMethod);
                    $controller->processRequest();
                    break;
                case 'types':
                    $controller = new Types($id, $requestMethod);
                    $controller->processRequest();
                    break;
                default:
                    $response['status_code_header'] = 'HTTP/1.1 200 OK';
                    $response['body'] = json_encode([
                        'success' => true,
                        'message' => 'Selamat Datang di Restful API Manajemen Sampah'
                    ]);
                    header($response['status_code_header']);
                    echo $response['body'];
                    break;
            }
        }
    }
}
