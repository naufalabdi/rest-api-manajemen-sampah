<?php

namespace Controller;

use Model\TypesModel;

class Types
{
    private $requestMethod;
    private $id;
    private $model;

    public function __construct($id, $requestMethod)
    {
        $this->id = $id;
        $this->requestMethod = $requestMethod;
        $this->model = new TypesModel();
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
        case 'GET':
          if ($this->id) {
              $response = $this->show($this->id);
          } else {
              $response = $this->index();
          }
          break;
        case 'POST':
          $response = $this->create();
          break;
        case 'PUT':
          $response = $this->update($this->id);
          break;
        case 'DELETE':
          $response = $this->delete($this->id);
          break;
        default:
          $response = $this->notFoundResponse();
          break;
      }

        header($response['status_code_header']);
        if (isset($response['body'])) {
            echo $response['body'];
        }
    }

    public function index()
    {
        $result = $this->model->getAll();
        if (! $result) {
            return $this->notFoundResponse();
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    public function show($id)
    {
        $result = $this->model->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function create()
    {
        $input = (array) json_decode(file_get_contents('php://input'), true);
        if (! $this->validate($input)) {
            return $this->unprocessableEntityResponse();
        }
        
        $result = $this->model->create($input);

        if (! $result) {
            return $this->internalError();
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode([
          'success' => true,
          'message' => 'Data Berhasil Disimpan'
        ]);
        return $response;
    }

    private function update($id)
    {
        $type = $this->model->find($id);
        if (! $type) {
            return $this->notFoundResponse();
        }

        $input = (array) json_decode(file_get_contents('php://input'), true);

        if (! $this->validate($input)) {
            return $this->unprocessableEntityResponse();
        }

        $result = $this->model->update($id, $input);

        if (! $result) {
            return $this->internalError();
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode([
          'success' => true,
          'message' => 'Data Berhasil Diubah'
        ]);
        return $response;
    }

    private function delete($id)
    {
        $material = $this->model->find($id);
        if (! $material) {
            return $this->notFoundResponse();
        }

        $result = $this->model->delete($id);

        if (! $result) {
            return $this->internalError();
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode([
          'success' => true,
          'message' => 'Data Berhasil Dihapus'
        ]);
        return $response;
    }

    private function validate($input)
    {
        if (! isset($input['material_id']) || $input['material_id'] === "") {
            return false;
        }

        if (! isset($input['name']) || $input['name'] === "") {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'success' => false,
            'message' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = json_encode([
          'success' => false,
          'message' => 'Method Tidak Ditemukan'
        ]);
        return $response;
    }

    private function internalError()
    {
        $response['status_code_header'] = 'HTTP/1.1 500 Internal Server Error';
        $response['body'] = json_encode([
        'success' => false,
        'message' => 'Terjadi Kesalahan Sistem'
      ]);
        return $response;
    }
}
