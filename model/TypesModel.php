<?php

namespace Model;

use Config\Database;

use Model\Models;

class TypesModel implements Models
{
    private $db;

    public function __construct()
    {
        $this->db = (new Database())->connect();
    }

    public function getAll()
    {
        $query = "
			SELECT 
				id, material_id, name
			FROM 
				types;
		";

        try {
            $statement = $this->db->query($query);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return $result;
    }

    public function find($id)
    {
        $query = "
			SELECT 
				id, material_id, name
			FROM 
				types
			WHERE id = :id;
		";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array('id' => $id));
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return $result;
    }

    public function create($input)
    {
        $query = "
        INSERT INTO types
            (id, material_id, name)
        VALUES 
            (:id, :material_id, :name);
        ";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array(
                'id'	 => $input['id'],
                'material_id' => $input['material_id'],
                'name'   => $input['name']
            ));
            $statement->rowCount();
            $result = true;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
        return $result;
    }

    public function update($id, $input)
    {
        $query = "
        UPDATE `types`
        SET 
            `id` = :id,
            `material_id` = :material_id
            `name`  = :name
        WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array(
                'id'	 => $id,
                'material_id' => $input['material_id'],
                'name'   => $input['name']
            ));
            $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
        return $statement;
    }

    public function delete($id)
    {
        $query = "
            DELETE FROM types
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array('id' => $id ));
            $statement->rowCount();
            $result = true;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
        return $result;
    }

    public function count()
    {
        $query = "
        SELECT 
            COUNT(*) as count
        FROM 
            types;
        ";

        try {
            $statement = $this->db->query($query);
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return $result;
    }
}
