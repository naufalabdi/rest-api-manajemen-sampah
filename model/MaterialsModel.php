<?php

namespace Model;

use Config\Database;

use Model\Models;

class MaterialsModel implements Models
{
    private $db;

    public function __construct()
    {
        $this->db = (new Database())->connect();
    }

    public function getAll()
    {
        $query = "
			SELECT 
				id, name
			FROM 
				materials;
		";

        try {
            $statement = $this->db->query($query);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response['body'] = json_encode($result);
        } catch (\PDOException $e) {
            $response['status_code_header'] = 'HTTP/1.1 500 Internal Server Error';
            $response['body'] = json_encode($e->getMessage());
        }

        return $response;
    }

    public function find($id)
    {
        $query = "
			SELECT 
				*
			FROM 
				materials
			WHERE id = :id;
		";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array('id' => $id));
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return $result;
    }

    public function create($input)
    {
        $query = "
        INSERT INTO materials
            (id, name)
        VALUES 
            (:id, :name);
        ";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array(
                'id'	 => $input['id'],
                'name'   => $input['name']
            ));
            $statement->rowCount();
            $result = true;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
        return $result;
    }

    public function update($id, $input)
    {
        $query = "
        UPDATE materials
        SET 
            id = :id,
            name  = :name
        WHERE id = :id
        ";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array(
                'id'	 => $id,
                'name'   => $input['name']
            ));
            $statement->rowCount();
            $result = true;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
        return $result;
    }

    public function delete($id)
    {
        $query = "
            DELETE FROM materials
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute(array('id' => $id ));
            $statement->rowCount();
            $result = true;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
        return $result;
    }

    public function count()
    {
        $query = "
        SELECT 
            COUNT(*) as count
        FROM 
            materials;
        ";

        try {
            $statement = $this->db->query($query);
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return $result;
    }
}
