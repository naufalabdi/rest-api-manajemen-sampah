<?php

require 'vendor/autoload.php';

use Src\App;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeload();

$app = new App;
